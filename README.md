# Scripts di creazione ambiente ed user Chroot "jail"
# Contenuto:

 - chroot_scripts/
	- create_chroot_env.sh: creazione ambiente
	- create_chroot_user_sh: creazione user 
 
 - /
	- create_jail.sh: creazione ambiente base "alternativo"
	- iptables_chroot.sh: regole di traffico uscita ipv4 e ipv6 per l'user in jail
	- l2chroot: cerca le librerie di un binario e le copia nell'ambiente

#
