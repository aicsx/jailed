#!/bin/bash
# Crea il set di directory e files per un ambiente chroot a funzioni limitate.
# Testato su debian 9.5 e relative directory librerie
#
# Uso: ./create_chroot_env.sh <directory>
#
# Basato sullo script originale di :
# (c) 2002 Javier Fernandez-Sanguino <jfs@computer.org>
# edited: ax

[ -z "$1" ] && { echo "Uso: $0 directory"; exit 1; }

id=`/usr/bin/id -u`
[ "$id" -gt 0 ] && { echo "Attenzione: Devi essere root affinche' mknod funzioni"; }

dir=$1
[ -e "$dir" ] && { echo "ERRORE: $dir esiste. specifica una directory non esistente"; exit 1; }

curdir=`/bin/pwd`

# Crea la directory chroot
/bin/mkdir -p $dir
cd $dir

# Crea il set di directory base
for i in bin dev etc lib home ; do
 /bin/mkdir -p $i
done

# Copia nell'ambiente chroot i file base della directory etc
for etc in /etc/hosts /etc/services /etc/nsswitch.conf /etc/resolv.conf ; do
 if [ "$etc" ] 
  then
   cp -a $etc etc/
 fi
done

# Copia nell'ambiente chroot gli eseguibili necessari
for cmd in /bin/ls /bin/pwd /bin/true /bin/false /bin/rbash /bin/bash /usr/bin/ssh /usr/bin/dircolors ; do
 if [ "$cmd" ] 
  then
   cp -a $cmd bin/
 fi
done

# Copia nell'ambiente chroot le librarie necessarie agli eseguibili scelti
for lib in /lib/x86_64-linux-gnu/ld* /lib/x86_64-linux-gnu/libacl* /lib/x86_64-linux-gnu/libattr* /lib/x86_64-linux-gnu/libc.* /lib/x86_64-linux-gnu/libc-* /lib/x86_64-linux-gnu/libdl* /lib/x86_64-linux-gnu/libncurse* /lib/x86_64-linux-gnu/librt* /lib/x86_64-linux-gnu/libpthread* /lib/x86_64-linux-gnu/libcrypt* /lib/x86_64-linux-gnu/libnsl* /lib/x86_64-linux-gnu/libz* /lib/x86_64-linux-gnu/libpam* /lib/x86_64-linux-gnu/libresolv* /lib/x86_64-linux-gnu/libutil* /usr/lib/x86_64-linux-gnu/libgssapi_krb5* /usr/lib/x86_64-linux-gnu/libkrb5* /usr/lib/x86_64-linux-gnu/libk5crypto* /lib/x86_64-linux-gnu/libcom_err* /lib/x86_64-linux-gnu/libselinux* /lib/x86_64-linux-gnu/libsepol* /usr/lib/x86_64-linux-gnu/libcrypto* /lib/x86_64-linux-gnu/libnss_* ; do
 if [ "$lib" ]
  then
   cp -a $lib lib/
 fi 
done

# Crea nell'ambiente chroot i devices necessari
cd dev
# Consoles
/bin/mknod -m 644 tty1 c 4 1
/bin/mknod -m 644 tty2 c 4 2
/bin/mknod -m 644 tty3 c 4 3
/bin/mknod -m 644 tty4 c 4 4
/bin/mknod -m 644 tty5 c 4 5
/bin/mknod -m 644 tty6 c 4 6
# Terminale per ssh e telnet
/bin/mknod -m 644 ttyp0 c 3 0
/bin/mknod -m 644 ttyp1 c 3 1
/bin/mknod -m 644 ttyp2 c 3 2
/bin/mknod -m 644 ttyp3 c 3 3
/bin/mknod -m 644 ttyp4 c 3 4
/bin/mknod -m 644 ttyp5 c 3 5
/bin/mknod -m 644 ttyp6 c 3 6
# Altri devices
/bin/mknod -m 444 urandom c 1 9
/bin/mknod -m 666 zero c 1 5
/bin/mknod -m 666 null c 1 3

# Torna alla directory di partenza
cd $curdir

exit 0
