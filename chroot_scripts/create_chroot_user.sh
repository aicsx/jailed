# ax
# Crea utente, effettua spostamenti vari per chroot ed assegna una password

chrootdir=/home/chroot

if  [ -z $1 ]
	 then
		   echo "Manca il nome utente da creare!"
		     exit 1
	     fi

	     # Aggiunge l'utente
	     useradd -s /bin/bash -m -d /home/$1 -g users $1

	     # Sposta la home nell'ambiente chroot
	     mv /home/$1 $chrootdir/home/

	     # Assegna i permessi alla home dell'utente
	     chmod 700 $chrootdir/home/$1/

	     # Aggiunge la riga relativa all'utente al passwd del chroot
	     cat /etc/passwd | grep "^$1" >> $chrootdir/etc/passwd

	     # Modifica la home al passwd di sistema
	     usermod -d $chrootdir/./home/$1 $1

	     # Chiede la password per l'utente appena creato
	     passwd $1

	     exit 0

