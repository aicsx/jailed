# Abilito per l'uid dell'user "test:users" l'uscita da eth0 solo per DNS e oidentd
iptables -t mangle -A OUTPUT -p udp -o eth0 --dport 53 -m owner --uid-owner 1002 -j ACCEPT
iptables -t mangle -A OUTPUT -p tcp -o eth0 --dport 113 -m owner --uid-owner 1002 -j ACCEPT
# Blocco tutto il resto in uscita su ipv4 "interfaccia eth0 principale"
iptables -t mangle -A OUTPUT -o eth0 -m owner --uid-owner 1002 -j DROP

# IPV6
# Abilito per il v6 scelto sull'interfaccia eth4 il traffico in uscita
ip6tables -t mangle -A OUTPUT -s 2a04:3545:1000:720:94:a6ff:fef1:56ff -o eth4 -m owner --uid-owner 1002 -j ACCEPT
# Impedisco all'uid del'user test il traffico in uscita su tutte le altre interfacce
ip6tables -t mangle -A OUTPUT -m owner --uid-owner 1002 -j DROP
